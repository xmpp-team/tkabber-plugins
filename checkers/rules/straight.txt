Straight Checkers or English Draughts

Rules of Play

1. Straight checkers, also known as English draughts, is played on the dark
squares only of a standard checkerboard of 64 alternating dark and light
squares, (eight rows, eight files) by two opponents having 12 checkers each of
contrasting colors, nominally referred to as black andwhite.

(Serious checker players generally use red and white checkers, and green and
yellow checkerboards.  These colors have been designated as official by the
American Checker Federation.  In any case, colors of the checkers and the board
should be different in order to provide good contrast, and especially to avoid
such combinations as black checkers on black squares.)

2. The board is positioned squarely between the players and turned so that a
dark square is at each player's near left side.  Each player places his
checkers on the dark squares of the three rows nearest him.  The player with
the darker checkers makes the first move of the game, andthe players take turns
thereafter, making one move at a time.

3. The object of the game is to prevent the opponent from being able to move
when it is his turn to do so.  This is accomplished either by capturing all of
the opponent's checkers, or by blocking those that remain so that none of them
can be moved.  If neither player can accomplish this, the game is a draw.

4. Single checkers, known as men, move forward only, one square at a time in a
diagonal direction, to an unoccupied square.  Men capture by jumping over an
opposing man on a diagonally adjacent square to the square immediately beyond,
but may do so only if this square is unoccupied.  Men may jump forward only,
and may continue jumping as long as they encounter opposing checkers with
unoccupied squares immediately beyond them.  Men may never jump over checkers
of the same color.

5. A man which reaches the far side of the board, whether by means of a jump or
a simple move, becomes a king, and the move terminates.  The opponent must then
crown the new king by placing a checker of the same color atop it.  A player is
not permitted to make his own move until he crowns his opponent's king.

6. Kings move forward or backward, one square at a time in a diagonal direction
to an unoccupied square.  Kings capture by jumping, forward or backward, over
an opposing man or king on a diagonally adjacent square to the square
immediately beyond, but may do so only if this square is unoccupied.  Kings may
continue jumping as long as they encounter opposing checkers with unoccupied
squares immediately beyond them.  Kings may never jump over checkers of the
same color.  They may never jump over the same opposing man or king more than
once.

7. Whenever a player is able to make a capture he must do so.  When there is
more than one way to jump, a player may choose any way he wishes, not
necessarily the one which results in the capture of the greatest number of
opposing units.  However, once a player chooses asequence of captures, he must
make all the captures possible in that sequence.  He may not leave one or more
checkers uncaptured that he could capture simply by continuing to jump.  A
&quot;huff&quot; of a checker for failure to jump properly is not permitted as
it was in the past.  Theincorrect move must be retracted, and a correct move
must be made.  If possible, the correct move must be made with the man or king
originally moved incorrectly.

8. Time limits for play may be based on a fixed amount of time for each move,
with less time allowed for situations in which there is one, and only one,
capturing move possible, or on a fixed amount of time for a given number of
moves, without regard to how much of this time is used on any one move.  When
the latter method is used, and the given number of moves has been made by each
player, with neither having used up the allotted time, an additional allotment
of time and moves is given to each.  This continues until the conclusion of the
game.  Unused time is retained when a new allotment is given.  A player loses a
game if his time expires before he has completed the required number of moves.
