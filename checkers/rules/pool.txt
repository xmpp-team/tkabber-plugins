Pool Checkers

Rules of Play

1. Pool checkers, also known as Spanish pool checkers, is played on the dark
squares only of a standard checkerboard of 64 alternating dark and light
squares, (eight rows, eight files) by two opponents having 12 checkers each of
contrasting colors, nominally referred to as black and white.

2. The board is positioned squarely between the players and turned so that a
dark square is at each player's near left side.  Each player places his
checkers on the dark squares of the three rows nearest him.  The player with
the darker checkers makes the first move of the game, and the players take
turns thereafter, making one move at a time.

3. The object of the game is to prevent the opponent from being able to move
when it is his turn to do so.  This is accomplished either by capturing all of
the opponent's checkers, or by blocking those that remain so that none of them
can be moved.  If neither player can accomplish this, the game is a draw.

4. Single checkers, known as men, move forward only, one square at a time in a
diagonal direction, to an unoccupied square.  Men capture by jumping over an
opposing man on a diagonally adjacent square to the square immediately beyond,
but may do so only if this square is unoccupied.  Men may jump forward or
backward, and may continue jumping as long as they encounter opposing checkers
with unoccupied squares immediately beyond them.  Men may never jump over
checkers of the same color.

5. A man which reaches the far side of the board becomes a king.  However, if
it reaches the far side by means of a jump, and is able to jump backward away
from the far side over another man or king, it must do so, and does not become
a king.  A man reaching the far side by jumping becomes a king only if its
jump, or series of jumps, terminates there.  When a man becomes a king the turn
to move passes to the other player, who must crown the new king by placing a
checker of the same color atop it.  A player is not permitted to make his own
move until he crowns his opponent's king.

6. Kings move forward or backward any number of squares on a diagonal line to
an unoccupied square.  Kings capture from any distance along a diagonal line by
jumping, forward or backward, over an opposing man or king with at least one
unoccupied square immediately beyond it.  The capturing king then lands on any
one of these unoccupied squares (except as noted in rule 7) and continues
jumping, if possible, either on the same line, or by making a right angle turn
onto another diagonal line.  Kings may never jump over checkers of the same
color.

7. Whenever a player is able to make a capture he must do so.  When there is
more than one way to jump, a player may choose any way he wishes, not
necessarily the one which results in the capture of the greatest number of
opposing units.  When a king jumps over an opposing man or king with more than
one unoccupied square immediately beyond it, it must land on a square from
which it is possible to continue jumping, if there is such a square.  If there
is more than one such square, any may be chosen.  However, once a player
chooses a sequence of captures, he must make all the captures possible in that
sequence.  He may not leave one or more checkers uncaptured that he could
capture simply by continuing to jump.  A "huff" of a checker for
failure to jump properly is not permitted as it was in the past.  The incorrect
move must be retracted, and a correct move must be made.  If possible, the
correct move must be made with the man or king originally moved incorrectly.

8. A man or king may not jump over the same opposing man or king more than
once.

9. Captured checkers are not removed from the board until all jumps made on the
move are completed, and the hand is removed from the capturing man or king.

(It is only in rare instances that rules 8 or 9 affect the play of a game.
They can have the effect of reducing the number of captures possible on a move.
In most of these cases a king is the capturing piece.  On very rare occasions
these rules, either separately or in combination, will result in a king being
forced to terminate a series of jumps on a square from which it will then be
captured by an opposing man or king which itself would have been captured were
it not for these rules.)

10. Whenever a situation arises in which one player has three kings and the
other one king, no other checkers remaining on the board, a count is begun of
the moves made by the lone king.  If the lone king is able to make 13 moves the
game is a draw, even if the next move by the opponent would be the capture of
the lone king.  (In general, a win is possible only if the side with three
kings has possession of the diagonal line running from the lower left corner to
the upper right corner.)

11. Time limits for play may be based on a fixed amount of time for each move,
with less time allowed for situations in which there is one, and only one,
capturing move possible, or on a fixed amount of time for a given number of
moves, without regard to how much of this time is used on any one move.  When
the latter method is used, and the given number of moves has been made by each
player, with neither having used up the allotted time, an additional allotment
of time and moves is given to each.  This continues until the conclusion of the
game.  Unused time is retained when a new allotment is given.  A player loses a
game if his time expires before he has completed the required number of moves.
