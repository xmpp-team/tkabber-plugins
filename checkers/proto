Request:

<iq type='set' to='a@b.c' id='id1'>
    <create xmlns='games:board' id='123' type='checkers:russian' board='8x8'
            color='white'>
        <timelimit>600</timelimit>   -- for all players
        <timelimit color='black'>10</timelimit>
                                     -- if on black side played Kasparov
        <initboard>
            <set pos='0,0' color='white' type='rook'/>
            <set pos='1,0' color='white' type='knight'/>
        </initboard>
    </create>
</iq>

Known types: checkers:russian, checkers:pool, checkers:spanish,
checkers:straight, checkers:italian

Positive response:

<iq type='result' to='x@y.z' id='id1'>
    <create xmlns='games:board' type='checkers:russian' id='123'/>
</iq>

or simply <iq type='result' id='id1'/>?


Move:

<iq type='set' to='a@b.c' id='id2'>
    <turn xmlns='games:board' type='checkers:russian' id='123'>
        <move pos='1,1;2,2'/>
    </turn>
</iq>

'pos' attribute contain column-separated list of coordinates of piece, that
moved from first to last one.  Coordinate is a comma-separated list of
integers >= 0.

Jump move:

<iq type='set' to='a@b.c' id='id2'>
    <turn xmlns='games:board' type='checkers:russian' id='123'>
        <move pos='1,1;3,3;5,1;7,3'/>
    </turn>
</iq>

Proposing draw:

<iq type='set' to='a@b.c' id='id3'>
    <turn xmlns='games:board' type='checkers:russian' id='123'>
        <move pos='1,7;3,5'/>
        <draw/>
    </turn>
</iq>

Accepting draw proposal:

<iq type='set' to='a@b.c' id='id4'>
    <turn xmlns='games:board' type='checkers:russian' id='123'>
        <accept/>
    </turn>
</iq>

Resigning:

<iq type='set' to='a@b.c' id='id5'>
    <turn xmlns='games:board' type='checkers:russian' id='123'>
        <resign/>
    </turn>
</iq>


Response:

<iq type='result' to='x@y.z' id='id2'/>

if move accepted, or error 'Not Acceptable' if other side think that this move
illegal (to avoid evil hackers that write clients that can do few moves instead
of one or can move King like Queen)
