# iconsets.tcl --
#
#       Several iconsets, borrowed from other Jabber clients.

package require msgcat

namespace eval iconsets {
    ::msgcat::mcload [file join [file dirname [info script]] msgs]

    if {![::plugins::is_registered iconsets]} {
        ::plugins::register iconsets \
                            -namespace [namespace current] \
                            -source [info script] \
                            -description [::msgcat::mc "Whether the Iconsets\
                                                        plugin is loaded."] \
                            -loadcommand \
                                [namespace code \
                                    [list load [file dirname [info script]]]] \
                            -unloadcommand [namespace code unload]
        return
    }
}

proc iconsets::load {dirname} {
    set ::pixmaps::theme_dirs \
        [concat $::pixmaps::theme_dirs \
                [glob -nocomplain -directory $dirname *]]

    ::pixmaps::load_themes
    ::pixmaps::init_custom
    ::pixmaps::load_stored_theme
}

proc iconsets::unload {} {
    set ::pixmaps::theme_dirs \
        [concat [glob -nocomplain -directory \
                      [file join $::configdir pixmaps] *] \
                [glob -nocomplain -directory [fullpath pixmaps] *]]

    ::pixmaps::load_themes
    ::pixmaps::init_custom
    ::pixmaps::load_stored_theme
}

# vim:ft=tcl:ts=8:sw=4:sts=4:et
