As usual, copy or link this directory to $HOME/.tkabber/plugins (on UNIX),
to %APPDATA%\Tkabber\plugins (on Windows), or to
$HOME/Library/Application Support/Tkabber (on MacOS X) directory.

Restart Tkabber, then you'll find OTR submenu in Tkabber top menu, in
users' roster menus and in chat tabs or message windows.

On all systems this plugin requires Tcl 8.5 or newer because it uses an
arbitrary precision integers. Also, the base64, sha1, sha256, aes, asn
and math::bignum packages are required, they all are bundled with the
Tcllib collection.

For Windows this plugin also requires the Memchan package. Its [random]
channel is used as a PRNG.

To actually use OTR you'll need a 1024 bit DSA private key (OTR protocol
supports only 1024 bit keys). There are two ways of making it work:

1) You can generate a new key directly in Tkabber (Main menu ->
Tkabber -> OTR -> Manage private keys)

2) You can import an existing key stored in PEM format.
To generate it outside Tkabber you can use OpenSSL:

openssl dsaparam -out dsaparam.pem 1024
openssl gendsa -out otr.private.key dsaparam.pem

Happy hacking!
