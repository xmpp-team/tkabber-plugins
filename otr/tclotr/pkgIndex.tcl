# pkgIndex.tcl --
#
#       This file is a part of Off-the-Record messaging protocol
#       implementation. It loads the main package.
#
# Copyright (c) 2014 Sergei Golovan <sgolovan@nes.ru>
#
# See the file "license.terms" for information on usage and redistribution
# of this file, and for a DISCLAMER OF ALL WARRANTIES.

package ifneeded otr 0.1 [list source [file join $dir otr.tcl]]
package ifneeded otr::key 0.1 [list source [file join $dir key.tcl]]
package ifneeded otr::data 0.1 [list source [file join $dir data.tcl]]
package ifneeded otr::crypto 0.1 [list source [file join $dir crypto.tcl]]
package ifneeded otr::auth 0.1 [list source [file join $dir auth.tcl]]
package ifneeded otr::smp 0.1 [list source [file join $dir smp.tcl]]
package ifneeded otr::message 0.1 [list source [file join $dir message.tcl]]

# vim:ts=8:sw=4:sts=4:et
