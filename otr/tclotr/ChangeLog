2023-03-30  Sergei Golovan  <sgolovan@nes.ru>

	* crypto.tcl: Use TWAPI to generate random numbers in Windows if
	  available.

2017-03-27  Sergei Golovan  <sgolovan@nes.ru>

	* otr.tcl: Use zero padding when formatting an OTR fingerprint for
	  display.

2016-01-04  Sergei Golovan  <sgolovan@nes.ru>

	* otr.tcl: Reset the keys counter after the AKE completion in case
	  when the correspondent's keys have been changed.

2016-01-02  Sergei Golovan  <sgolovan@nes.ru>

	* otr.tcl: Fixed a small bug in returning "can't decrypt" message.
	  Added -discardcommand and -resendcommand options to the otr::new
	  procedure. They allow the caling application to replace built-in
	  storing, discarding and resending routines.

2015-12-27  Sergei Golovan  <sgolovan@nes.ru>

	* otr.tcl: Don't switch to plaintext message state on an OTR error
	  message retrieving. Display a bit more meaningful error messages
	  to a user in case of receiving OTR request with nismatched protocol
	  version. Do not resend pending message if the peer has changed her
	  OTR key between sessions.

2015-05-10  Sergei Golovan  <sgolovan@nes.ru>

	* auth.tcl, message.tcl, otr.tcl: Removed trailing spaces.

2015-05-09  Sergei Golovan  <sgolovan@nes.ru>

	* crypto.tcl, key.tcl, otr.tcl, pkgIndex.tcl: Reformatted the TclOTR
	  library code.

2015-03-29  Sergei Golovan  <sgolovan@nes.ru>

	* auth.tcl, crypto.tcl, data.tcl, key.tcl, message.tcl, otr.tcl,
	  pkgIndex.tcl, smp.tcl: Removed no longer needed SVN keywords
	  placeholders.

	* ChangeLog, README: Added separate ChangelLog and README for the
	  TclOTR package.

2015-03-25  Sergei Golovan  <sgolovan@nes.ru>

	* otr.tcl: Returned [eval] instead of [uplevel #0] for
	  calling back since [eval] allows to look up the call stack anf find
	  something useful there (see [otr::send] procedure).
	  Switch the OTR state to plaintext after an OTR error message is
	  received. Otherwise, if the other side disables OTR in the middle of
	  an encrypted conversation we cannot tell her anything at all - all
	  messages will be encrypted, and she'll never read them.

	* license.terms: Fixed copyright year.

2014-11-12  Sergei Golovan <sgolovan@nes.ru>

	* otr.tcl: Evaluate external callback procedures at the top
	  stack level.

2014-05-06  Sergei Golovan <sgolovan@nes.ru>

	* auth.tcl: Fixed recreating the reveal signature message.

2014-02-19  Sergei Golovan <sgolovan@nes.ru>

	* smp.tcl: Added a debug print procedure instead of plain
	  puts calls.

2014-02-15  Sergei Golovan <sgolovan@nes.ru>

	* README: Fixed the requirements list.

2014-02-02  Sergei Golovan <sgolovan@nes.ru>

	* key.tcl: Moved key generation procedure to
	  the otr::key package.

2014-01-27  Sergei Golovan <sgolovan@nes.ru>

	* key.tcl: Fixed typo.

	* key.tcl: Use unix line endings when exporting the DSA
	  private key. Refuse to import keys which aren't 1024 bit long.

	* otr/README: Fixed info on how to use private keys.

	* message.tcl: Treat human readable message part as HTML
	  (strip tags for incoming messages, escape HTML entities for
	  outgoing ones).

2014-01-26  Sergei Golovan <sgolovan@nes.ru>

	* otr.tcl: Fixed refreshing private connection if the
	  peer has changed his instance tag. Clear messages subject to resend
	  when the message state switches from encrypted.

	* auth.tcl: Fixed recreating D-H commit message.

2014-01-25  Sergei Golovan <sgolovan@nes.ru>

	* key.tcl, crypto.tcl: Code cleanup, made DSA key
	  generation more clear.

	* otr.tcl: Use the sender's instance tag
	  included into the Diffie-Hellman commit message. Resend the last
	  data message if it was replied by an OTR error message (resend it
	  after a new successful AKE).

	* otr.tcl: Fixed processing message state changes. Show
	  an info message when private conversation is started.

	* otr.tcl: Show an info message when private conversation
	  is finished.

	* otr.tcl: Implemented functions which return SSID and
	  fingerprint.

2014-01-24  Sergei Golovan <sgolovan@nes.ru>

	* crypto.tcl: Implemented generating new
	  DSA private key.

2014-01-23  Sergei Golovan <sgolovan@nes.ru>

	* message.tcl, otr.tcl: Code
	  cleanup. Moved most of the interaction with the calling program to
	  callbacks.

	* otr.tcl: Fixed typos in callback invocations.

2014-01-21  Sergei Golovan <sgolovan@nes.ru>

	* key.tcl, pkgIndex.tcl: Added a new subpackage
	  which currently encodes and decodes DSA private keys in PEM format.

	* otr.tcl: Tell the OTR plugin about state
	  changes and SMP progress using callbacks and only in case of actual
	  change.

	* otr.tcl: Attach the OTR whitespace tag only if there
	  weren't received plaintext messages from the peer before.

2014-01-20  Sergei Golovan <sgolovan@nes.ru>

	* otr.tcl: Do not send the OTR query message after an OTR
	  error if no ALLOW_V2 or ALLOW_V3 is in the policy flags.

	* data.tcl: Removed already done todo item.

	* otr.tcl, message.tcl: Implemented the old MAC
	  keys revelation, checking for the peer's counter monotonicity,
	  ignoring verification and decryption errors if IGNORE_UNREADABLE
	  message flag is set.

	* data.tcl, otr.tcl: Implemented assembling
	  incoming messages from fragments.

	* otr.tcl: Implemented sending heartbeat messages.

	* otr.tcl: Fixed typo in code which stores old MAC keys.

	* otr.tcl: Removed unnecessary field from
	  OTR procedures' result.

	* otr.tcl: Fixed typo in ::otr::configure.

	* otr.tcl: Warn user if plaintext message
	  is received while encryption is required.

2014-01-19  Sergei Golovan <sgolovan@nes.ru>

	* otr.tcl: Fixed checking data message hash for protocol
	  version 3.

	* message.tcl: Fixed typo in processing SMP message 4.
	  Fixed error reporting.

	* smp.tcl: SMP protocol is now working, but its outcome
	  isn't utilised anywhere.

	* message.tcl: Got rid of eval and uplevel calls.

	* otr.tcl: Don't attach the OTR whitespace tag if policy
	  doesn't allow using any protocol version.

	* message.tcl, otr.tcl, smp.tcl: Roughly finished SMP implementation.
	  No peer authentication status storing yet though.

2014-01-18  Sergei Golovan <sgolovan@nes.ru>

	* *: Better error reporting. Continue implementing peer
	  authentication.

2014-01-17  Sergei Golovan <sgolovan@nes.ru>

	* *: Added pre-alpha of the new OTR package. No key
	  management, peer authentication, SMP, proper error reporting,
	  resending messages yet. Not for regular usage yet.

	* crypto.tcl: Added procedure which computes binary
	  DSA public key fingerprint.

	* otr.tcl: Don't compute DSA key fingerprint directly, but
	  convert the one computed in the otr::crypto package.

	* smp.tcl: Initially implemented parsing, creating and
	  verifying OTR SMP messages.
