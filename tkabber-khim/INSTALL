As usually, copy this directory to $HOME/.tkabber/plugins (on UNIX),
to %APPDATA%\Tkabber\plugins (on Windows), or to
$HOME/Library/Application Support/Tkabber (on MacOS X) directory.

Restart Tkabber, then navigate to the "Services" -> "Plugins" menu
and activate the "KHIM Options..." menu entry -- the KHIM configuration
dialog will appear. Hit the "Help..." button and read the help carefully.

Normally, this plugin is bundled with the original "khim" and "autoscroll"
Tcl packages, which are parts of the "tklib" library [1].

If these packages are missing, you most probably need to get tklib installed
in your system. The khim package is not yet currently in the official "stable"
flavor of the tklib [2], so you generally have two options:

1) Recent Active State's distros come with the "CVS HEAD state" tklib, so
   if you have such a distro, you most probably have khim, too.

2) Fetch and install tklib from CVS:

   * Log in as anonymous with the empty password (just hit Enter):

     cvs -d :pserver:anonymous@tcllib.cvs.sourceforge.net/cvsroot/tcllib/ login

   * Fetch the sources (the "tklib" directory under the current one will
     be created):

     cvs -z3 -d :pserver:anonymous@tcllib.cvs.sourceforge.net/cvsroot/tcllib/ \
         co tklib

   * Log out:

     cvs -d :pserver:anonymous@tcllib.cvs.sourceforge.net/cvsroot/tcllib/ \
         logout

   * Install:

     cd tklib
     ./configure --with-tcl=/usr/bin --prefix=/usr
     make test
     su
     make install

     (Leave out "su" if not on Unix. "make test" step is optional, of course).

   * Alternatively, you can just grab "khim" and "autoscroll" packages and
     place them into the root directory of this plugin.

[1] http://tcllib.sourceforge.net/
[2] This is written on 09-Dec-2006.
