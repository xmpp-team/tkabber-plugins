# tkabber-khim.tcl --
#
#       Tkabber plugin adding KHIM functionality to Tkabber's Text and Entry
#       widgets.
#
# Author: Konstantin Khomoutov <flatworm@users.sourceforge.net>.
# KHIM author: Kevin B Kenny <kennykb@users.sourceforge.net>.
#
# Read more about KHIM: http://wiki.tcl.tk/16343
#
# To use this plugin you either need KHIM and autoscroll packegs themselves,
# or the fresh tklib, which KHIM is now a part of.
# Refer to INSTALL file for details.

# TODO eval'ing of KHIM settings looks unsafe.
# TODO it would be better not to subvert ::entry and ::text commands.

namespace eval khim {
    # If the directory named "khim" is available locally, use it.
    # This makes possible to distribute KHIM and autoscroll bundled
    # with this plugin (and lift the dependency on tklib).

    set scriptdir [file dirname [info script]]
    set local_khim [file join $scriptdir khim]

    if {[file isdirectory $local_khim]} {
        lappend ::auto_path $scriptdir
        package forget khim
    }

    # Load message catalog:

    package require msgcat
    ::msgcat::mcload [file join $scriptdir msgs]

    # Attempt to load KHIM. Warn and quit loading plugin if we can't:

    if {[catch { package require khim } err]} {
        # TODO why didn't tk_messageBox work here?
        puts stderr $err
        puts stderr [::msgcat::mc "Problem loading KHIM. Tkabber-khim\
                                   functionality will be\
                                   disabled.\nRefer to the INSTALL file\
                                   of the tkabber-khim plugin."]
        # Clean up what's already here and bail out:
        namespace delete [namespace current]
        return
    }

    # Configuration section:

    variable settings

    custom::defvar settings {} \
        "KHIM subsystem settings" \
        -group Hidden \
        -type string

    # Event handlers:

    # This handler should be called after Customize DB has been restored
    # which occurs in postload_hook at priority 60.
    ::hook::add postload_hook [namespace current]::restore_settings 70

    ::hook::add finload_hook  [namespace current]::on_init

    # The trickery (see http://wiki.tcl.tk/16343):
    auto_load Entry
    rename ::Entry ::khim::Entry
    rename ::text  ::khim::text

    proc ::Entry {w args} {
        eval [linsert $args 0 ::khim::Entry $w]
        bindtags $w [linsert [bindtags $w] 1 KHIM]
        return $w
    }

    proc ::text {w args} {
        eval [linsert $args 0 ::khim::text $w]
        bindtags $w [linsert [bindtags $w] 1 KHIM]
        return $w
    }
}


# Passes the KHIM settings restored from the Customize DB
# to the KHIM subsystem:
proc khim::restore_settings {args} {
    variable settings

    catch {eval $settings}
}


# Invokes the KHIM's options dialog,
# then saves current KHIM options.
# Intended to be invoked via Tkabber menus, etc.
proc khim::edit_options {} {
    variable settings

    ::khim::getOptions .tkabber_khim_options

    set settings [::khim::getConfig]
}


# Creates an entry for KHIM in the "Services->Plugins" Tkabber menu:
proc khim::on_init {} {
    set menu [.mainframe getmenu plugins]
    $menu add command \
          -label [::msgcat::mc "KHIM options"] \
          -command [namespace current]::edit_options
}

# vim:ts=8:sw=4:sts=4:et
