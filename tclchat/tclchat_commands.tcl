# tclchat_commands.tcl --
#
#       This file is a part of implementation of Tkabber's interface to
#       the Tcler's chat. It adds a few IRC-like commands.
#
# Copyright (C) 2004 Pat Thoyts <patthoyts@users.sf.net>
# Copyright (C) 2007-2009 Sergei Golovan <sgolovan@nes.ru>

namespace eval tclchat {}

proc tclchat::load_commands {} {
    hook::add chat_send_message_hook \
              [namespace current]::send_commands 50
    hook::add generate_completions_hook \
              [namespace current]::command_completion
}

proc tclchat::unload_commands {} {
    hook::remove chat_send_message_hook \
                 [namespace current]::send_commands 50
    hook::remove generate_completions_hook \
                 [namespace current]::command_completion
}

proc tclchat::command_completion {chatid compsvar wordstart line} {
    upvar 0 $compsvar comps
    if {!$wordstart} {
        lappend comps {/google }
        lappend comps {/googlefight }
        lappend comps {/tip }
        lappend comps {/bug }
    }
}

proc tclchat::send_commands {chatid user body type} {
    if {$type == "groupchat"} {
        set body [string trim $body]
        if {[string match /* $body]} {
            switch -regexp -- $body {
                {^/google\s} {
                    set body [string range $body 8 end]
                    debugmsg tclchat "google \"$body\""
                    if {[string length $body] > 0} {
                        set q {http://www.google.com/search?ie=UTF-8&oe=UTF-8&}
                        append q [::http::formatQuery q $body]
                        browseurl $q
                        return stop
                    }
                }
                {^/googlefight\s} {
                    googlefight $body
                    return stop
                }
                {^/(urn:)?tip[: ]\d+} {
                    if {[regexp {(?:urn:)?tip[: ](\d+)} $body -> tip]} {
                        browseurl http://tip.tcl.tk/$tip
                        return stop
                    }
                }
                {^/bug[: ]} {
                    dobug [split $body ": "]
                    return stop
                }
            }
        }
    }
}

proc tclchat::dobug {msg} {
    # msg should be of form: ^/bug[: ]id
    if {[llength $msg] != 2} {
        debugmsg tclchat "wrong # args: must be /bug id"
        return
    }
    set id  [lindex $msg end]
    set url "http://sourceforge.net/support/tracker.php?aid=$id"
    browseurl $url
}

proc tclchat::googlefight {msg} {
    set q {http://www.googlefight.com/cgi-bin/compare.pl}
    set n 1
    foreach word [lrange $msg 1 end] {
        append q [expr {($n == 1) ? "?" : "&"}]
        append q q$n=$word
        incr n
    }
    if {[string match fr_* [msgcat::mclocale]]} {
        append q &langue=fr
    } else {
        append q &langue=us
    }
    browseurl $q
}

# vim:ts=8:sw=4:sts=4:et
