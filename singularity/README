"Singularity" -- main interface plugin for Tkabber.


I. The Idea

When you chat with someone in Tkabber, the corresponding chat
window is opened for the full JID of that person (it is a JID
with the resource part: node@server/resource). Since your peers
can normally log into their XMPP accounts several times at once
(with a different resource each time) Tkabber maintains clear
distinction between these full JIDs.
On the other hand, most people seldom chat from several
connections to their account at once -- quite more often they
move between two or more XMPP clients located in places which
are geographically quite apart, such as their home and office.
In this case it may be convenient to maintain just one chat
window open for each person you're chatting with -- for
instance, you start chatting whith your peer when he/she is at
home, then your peer moves to their office and sends a message
from the new location; at this point it might be convenient not
to have two chat windows -- old and new -- open, but just the
new one.

This plugin implements the described functionality: when the new
window for a one-on-one chat is about to be opened the plugin
checks whether there are any active chat sessions with the same
bare JID (that is, without the "/resource" part) as that of the
new chat. If there are, they are closed with their conversation
log and input history preserved, the new chat window is opened
and then the saved data is inserted into the new window.
So effectively this plugin makes Tkabber keep just one chat
window per bare JID open.

The plugin does not touch MUC private chats because in them
resource part of the room JID is used to designate participants'
nicknames.


II. Usage

Just install this plugin as usually and restart Tkabber.
Currently there's no UI to control the plugin's behaviour.


III. Bugs and limitations

* Tkabber is currently unable to actually "switch" a chat window
  from one JID to another, so this plugin works by closing the
  existing window(s) and opening the new one. While in tabbed
  mode this looks mostly OK, in windowed mode this may be
  annoying because the new window will most probably not appear
  at the same location and layer as the closed window.

* For some unknown reason the first line of the input history is
  lost when the chat state is transferred to the newly opened
  window.

* If there are several chat windows to be closed when the new
  one is about to be opened, the order in which their
  conversation logs and input history records are collected
  is undefined.


# vim:tw=64:et
