# Tkabber plugins Makefile

PREFIX = /usr/local
TKABBERPLUGINSDIR = $(PREFIX)/share/tkabber-plugins
DOCDIR = $(PREFIX)/share/doc/tkabber-plugins

SUBDIRS = aniemoticons    \
	  attline         \
	  battleship      \
	  bc              \
	  browser         \
	  checkers        \
	  chess           \
	  ctcomp          \
	  custom-urls     \
	  cyrillize       \
	  debug           \
	  ejabberd        \
	  filters         \
	  floatingcontact \
	  floatinglog     \
	  georoster       \
	  gmail           \
	  iconsets        \
	  jidlink         \
	  latex           \
	  mute            \
	  openurl         \
	  osd             \
	  otr             \
	  poker           \
	  presencecmd     \
	  quiz            \
	  quotelastmsg    \
	  receipts        \
	  renju           \
	  reversi         \
	  singularity     \
	  socials         \
	  spy             \
	  stripes         \
	  tclchat         \
	  tkabber-khim    \
	  traffic         \
	  unixkeys        \
	  whiteboard

install: install-bin install-doc

install-bin:
	install -d $(DESTDIR)/$(TKABBERPLUGINSDIR)
	cp -dr --no-preserve=ownership $(SUBDIRS) $(DESTDIR)/$(TKABBERPLUGINSDIR)
	@echo ""
	@echo ""
	@echo ""
	@echo "To enable desired plugins copy or link their directories to ~/.tkabber/plugins/"
	@echo ""
	@echo ""
	@echo ""

install-doc:
	install -d $(DESTDIR)/$(DOCDIR)
	install -m 644 COPYING ChangeLog README $(DESTDIR)/$(DOCDIR)

# Update Tkabber plugins from Fossil repository
up:
	test -f .fslckout -o -f _FOSSIL_ && fossil pull && fossil update

.PHONY: install install-bin install-doc up
