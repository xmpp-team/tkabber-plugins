Request:

<iq type='set' to='a@b.c' id='id1'>
    <create xmlns='games:board' id='123' type='renju' board='8x8'
            color='white'>
        <timelimit>600</timelimit>   -- for all players
        <timelimit color='black'>10</timelimit>
                                     -- if on black side played Kasparov
        <initboard>
            <set pos='0,0' color='white'/>
            <set pos='1,0' color='white'/>
        </initboard>
    </create>
</iq>

Positive response:

<iq type='result' to='x@y.z' id='id1'>
    <create xmlns='games:board' type='renju' id='123'/>
</iq>

or simply <iq type='result' id='id1'/>?



Turn:

<iq type='set' to='a@b.c' id='id2'>
    <turn xmlns='games:board' type='renju' id='123'>
        <put pos='1,6'/>
    </turn>
</iq>

'pos' attribute contains coordinates of piece.
Coordinate is a comma-separated list of integers >= 0.

Skipping the move:

<iq type='set' to='a@b.c' id='id3'>
    <turn xmlns='games:board' type='renju' id='123'>
        <skip/>
    </turn>
</iq>

Proposing draw:

<iq type='set' to='a@b.c' id='id4'>
    <turn xmlns='games:board' type='renju' id='123'>
        <put pos='1,7'/>
        <draw/>
    </turn>
</iq>

Accepting draw proposal:

<iq type='set' to='a@b.c' id='id5'>
    <turn xmlns='games:board' type='renju' id='123'>
        <accept/>
    </turn>
</iq>

Resigning:

<iq type='set' to='a@b.c' id='id6'>
    <turn xmlns='games:board' type='renju' id='123'>
        <resign/>
    </turn>
</iq>

Also we need <put pos='2,10'/> and <destroy pos='2,10'/> tags for games where
we must put or remove pieces.  <put/> can have 'type' and 'color'
attributes.

move, put & destroy tags may have some game-specific subtags.



Response:

<iq type='result' to='x@y.z' id='id2'/>

if move accepted, or error 'Not Acceptable' if other side thinks that this move
illegal.
