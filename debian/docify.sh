#! /bin/sh

set -e -u

if [ $# -lt 2 ]; then
	echo "Usage: `basename $0` DOC_DIR PLUGIN_DIR [PLUGIN_DIR ...]" >&2
	exit 1
fi

DOCDIR=$1; shift

for d in $@; do
	find "$d" -type f \( \
		-iname '*licen[cs]e*' \
		-o -iname AUTHORS \
		-o -iname INSTALL \
		-o -iname TODO \
		-o -iname VERSION* \
		-o -iname ChangeLog \) -exec rm -f '{}' \;

	for f in ` find "$d" -type f \( \
			-iname README \
			-o -iname proto \
			\) `; do
		mv "$f" "$DOCDIR/`basename \"$f\"`.`basename $(dirname \"$f\")`"
	done
done

