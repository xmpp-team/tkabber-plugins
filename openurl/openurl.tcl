# openurl.tcl --
#
#       This file implements Open URL plugin for the Tkabber XMPP client.
#       It allows to choose in which browser you want to open an URL.

package require msgcat

namespace eval openurl {
    ::msgcat::mcload [file join [file dirname [info script]] msgs]

    if {![::plugins::is_registered openurl]} {
        ::plugins::register openurl \
                            -namespace [namespace current] \
                            -source [info script] \
                            -description [::msgcat::mc "Whether the Open URL\
                                                        plugin is loaded."] \
                            -loadcommand [namespace code load] \
                            -unloadcommand [namespace code unload]
        return
    }

    set options(browsers) [list \
        iceweasel "Iceweasel" \
        chromium "Chromium" \
        firefox "Firefox" \
        galeon "Galeon" \
        konqueror "Konqueror" \
        mozilla-firefox "Mozilla Firefox" \
        mozilla-firebird "Mozilla Firebird" \
        mozilla "Mozilla" \
        netscape "Netscape" \
        iexplore "Internet Explorer" \
        opera "Opera" \
        chrome "Google Chrome" \
        lynx "Lynx" \
        links "Links" \
        elinks "Elinks" \
        dillo "Dillo"]

    custom::defgroup Plugins [::msgcat::mc "Plugins options."] \
        -group Tkabber

    custom::defgroup {Open URL} [::msgcat::mc "Open URL plugin options."] \
        -group Plugins

    custom::defvar options(submenu) 1 \
        [::msgcat::mc "Use submenu for browsers list."] \
        -group {Open URL} \
        -type boolean

    custom::defvar options(custom_browser) "" \
        [::msgcat::mc "User defined browser\
                       name.\n\nExamples:\n  HV3\n  Google Chrome"] \
    -group {Open URL} \
    -type string

    custom::defvar options(path_to_custom_browser) "" \
        [::msgcat::mc "A binary name or a full path to user defined browser.\
                       Use its full path if can't be\
                       autodetected.\n\nExample:\n  ~/bin/hv3/hv3-linux"] \
    -group {Open URL} \
    -type string
}

proc openurl::load {} {
    hook::add chat_win_popup_menu_hook \
              [namespace current]::add_chat_win_popup_menu 5
}

proc openurl::unload {} {
    variable options

    hook::remove chat_win_popup_menu_hook \
                 [namespace current]::add_chat_win_popup_menu 5

    catch {unset options(browsers)}
}

proc openurl::open_url {brname brpath url} {
    switch -- $brname {
        iceweasel -
        firefox -
        mozilla-firefox -
        mozilla-firebird -
        mozilla -
        netscape {
            if {[catch {
                    exec $brpath -remote openURL($url,new-tab)
                 }]} {
                exec $brpath $url &
            }
        }
        galeon {
            exec $brpath --new-tab $url &
        }
        opera {
            exec $brpath -newpage $url &
        }
        links -
        elinks -
        lynx {
            exec xterm -e $brpath $url &
        }
        default {
            exec $brpath $url &
        }
    }
}

proc openurl::add_chat_win_popup_menu {m chatwin X Y x y} {
    variable options

    set tags [$chatwin tag names "@$x,$y"]
    if {[lsearch -glob $tags href_*] < 0} return

    set url \
        [::plugins::urls::encode_url [::plugins::urls::get_url $chatwin $x $y]]

    if {$options(submenu)} {
        set mb [menu $m.openurl -tearoff 0]

        foreach {brname brdesc} $options(browsers) {
            if {[llength [set e [auto_execok $brname]]] > 0} {
                $mb add command \
                    -label $brdesc \
                    -command [list [namespace current]::open_url \
                                   $brname [lindex $e 0] $url]
            }
        }
        if {$options(custom_browser) != "" && \
                                $options(path_to_custom_browser) != ""} {
            set brname $options(path_to_custom_browser)
            if {[llength [set e [auto_execok $brname]]] > 0} {
                $mb add command \
                    -label $options(custom_browser) \
                    -command [list [namespace current]::open_url \
                                   $brname [lindex $e 0] $url]
            }
        }

        $m add cascade -label [::msgcat::mc "Open URL with"] -menu $mb
    } else {
        foreach {brname brdesc} $options(browsers) {
            if {[llength [set e [auto_execok $brname]]] > 0} {
                $m add command \
                   -label [::msgcat::mc "Open URL with %s" $brdesc] \
                   -command [list [namespace current]::open_url \
                                  $brname [lindex $e 0] $url]
            }
        }
        if {$options(custom_browser) != "" && \
                                $options(path_to_custom_browser) != ""} {
            set brname $options(path_to_custom_browser)
            if {[llength [set e [auto_execok $brname]]] > 0} {
                $m add command \
                   -label [::msgcat::mc "Open URL with %s" \
                                        $options(custom_browser)] \
                   -command [list [namespace current]::open_url \
                                  $brname [lindex $e 0] $url]
            }
        }
    }
}

# vim:ts=8:sw=4:sts=4:et
